class CfgPatches
{
	class stuartisland
	{
		units[] = {"stuartisland"};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {"DZ_Data","DZ_Surfaces","DZ_Worlds_Chernarusplus_World"};
		worlds[] = {"stuartisland"};
	};
};

class cfgCharacterScenes
{
	class stuartisland
	{
		class loc1
		{
			target[] = {3266.0, 3022.0, 15};
			position[] = {3256.0, 3012.0, 1};
			fov = 0.9236;
			date[] = {2019, 10, 10, 10, 10};
			overcast = 0.15;
			rain = 0.0;
			fog = 0.2;
		};
	};
};

class CfgMissions
{
	class Cutscenes
	{
		class StuartislandIntro
		{
			directory = "stuartisland\data\scenes\intro.stuartisland";
		};
	};
};
class CfgWorlds
{
	class DefaultWorld
	{
		class Weather;
		class Grid;
	};
	class CAWorld: DefaultWorld
	{
		class Grid: Grid {};
		class Clutter;
	};
	class DefaultLighting;
	class DefaultClutter;
	class ChernarusPlus: CAWorld{};
	
	class stuartisland: ChernarusPlus
	{
		access = 3;
		cutscenes[] = {"StuartislandIntro"};
		description = "Stuart Island";
		worldName = "stuartisland\world\stuartisland.wrp";
		class Navmesh
		{
			#include "navmesh.hpp"
		};
		startTime="12:35";
		startDate="11/10/2018";
		class OutsideTerrain
		{
			satellite = "DZ\worlds\chernarusplus\data\outside_sat_co.paa";
			enableTerrainSynth = 1;
			class Layers
			{
				class Layer0
				{
					nopx = "DZ\surfaces\data\terrain\cp_gravel_nopx.paa";
					texture = "DZ\surfaces\data\terrain\cp_gravel_ca.paa";
				};
			};
		};
		startWeather=0.25;
		startFog=0;
		forecastWeather=0.25;
		forecastFog=0;
		centerPosition[]={2560,2560,300};
		
		ilsPosition[] = {3378.01,2972.4};
		ilsDirection[] = {0.9466,0.052336,-0.3225};
		ilsTaxiIn[] = {3051.44,3107.52,3366.52,2999.29,3384.980,2993.068,3398.981,2987.574,3407.249,2981.468,3408.818,2974.784,3405.065,2968.118,3396.824,2966.178};
		ilsTaxiOff[] = {3047.5,3085.14,3030.017,3090.859,3021.343,3096.703,3021.920,3103.445,3025.804,3108.88,3030.65,3111.5,3038.87,3112.03,3051.44,3107.52};
		drawTaxiway = 1;
		
		// override for sea materials (enfusion materials)
		oceanMaterial = "stuartisland\data\stuartisland_sea.emat";
		oceanNiceMaterial = "stuartisland\data\stuartisland_sea_nice.emat";
		oceanStormMaterial = "stuartisland\data\stuartisland_sea_storm.emat";
		
		ceFiles = "stuartisland\ce";
		
		class SecondaryAirports {};
		terrainNormalTexture="stuartisland\data\global_nohq.paa";
		minTreesInForestSquare=10;
		minRocksInRockSquare=5;
		
		class UsedTerrainMaterials
		{
			material0="stuartisland\data\surfaces\si_grass.rvmat";
			material1="stuartisland\data\surfaces\si_sand.rvmat";
			material2="DZ\surfaces\data\terrain\cp_grass.rvmat";
			material3="DZ\surfaces\data\terrain\cp_conifer_common2.rvmat";
			material4="DZ\surfaces\data\terrain\cp_dirt.rvmat";
			material5="DZ\surfaces\data\terrain\cp_concrete1.rvmat";
		};
		// 2d map grid
		class Grid
		{
			offsetX = 0.0;
			offsetY = 0.0;
			class Zoom1
			{
				zoomMax = 0.15;
				format = "XY";
				formatX = "000";
				formatY = "000";
				stepX = 100.0;
				stepY = 100.0;
			};
			class Zoom2
			{
				zoomMax = 0.85;
				format = "XY";
				formatX = "00";
				formatY = "00";
				stepX = 1000.0;
				stepY = 1000.0;
			};
			class Zoom3
			{
				zoomMax = 1e+030;
				format = "XY";
				formatX = "0";
				formatY = "0";
				stepX = 10000.0;
				stepY = 10000.0;
			};
		};
		class Names
		{
			#include "stuartisland.hpp"
			class PrevostAirstrip
			{
				name="Прево Взлетно-посадочная полоса";
				position[]={921.92,3783.75};
				type="Airport";
				
				radiusA=114.29;
				radiusB=88.23;
				angle=0.000;
			};
			class Stuart
			{
				name="Стюарт";
				position[]={2579.95,2384.77};
				type="Hill";
				
				radiusA=279.04;
				radiusB=215.41;
				angle=0.000;
			};
			class Prevost
			{
				name="Прево";
				position[]={1422.14,3796.04};
				type="NameCity";
				
				radiusA=545.01;
				radiusB=420.73;
				angle=0.000;
			};
		};
	};
};
class CfgWorldList
{
	class stuartisland {};
};
