class DefaultKeyPoint1
{
	name="Satallite Island";
	position[]={2506.09,3675.35};
	type="NameCity";
	
	radiusA=976.10;
	radiusB=760.19;
	angle=0.000;
};
class DefaultKeyPoint3
{
	name="Spieden Island";
	position[]={4001.97,1709.23};
	type="NameCity";
	
	radiusA=1220.13;
	radiusB=950.23;
	angle=0.000;
};
class Airport
{
	name="Stuart Island Airfield";
	position[]={3100.36,3049.87};
	type="Airport";
	
	radiusA=435.51;
	radiusB=336.57;
	angle=0.000;
};
class DefaultKeyPoint6
{
	name="Sentinel Island";
	position[]={3726.55,1489.21};
	type="NameVillage";
	
	radiusA=681.26;
	radiusB=525.91;
	angle=0.000;
};
class DefaultKeyPoint7
{
	name="Reef Bay";
	position[]={4362.90,2489.76};
	type="NameMarine";
	
	radiusA=223.23;
	radiusB=172.33;
	angle=0.000;
};
class DefaultKeyPoint2
{
	name="Stuart Island";
	position[]={1313.84,3301.64};
	type="NameCityCapital";
	
	radiusA=1220.13;
	radiusB=950.23;
	angle=0.000;
};
