		class BirdWalk_LookupTable
		{
			class BirdWalkstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"BirdWalk_Grass_dry_SoundSet"};
			};
			class BirdWalkstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"BirdWalk_Gravel_SoundSet"};
			};
		};
		class BirdGrazing_LookupTable
		{
			class BirdGrazingstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"BirdGrazing_Grass_dry_SoundSet"};
			};
			class BirdGrazingstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"BirdGrazing_Gravel_SoundSet"};
			};
		};
		class BirdBodyfall_LookupTable
		{
			class BirdBodyfallstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"BirdBodyfall_Grass_dry_SoundSet"};
			};
			class BirdBodyfallstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"BirdBodyfall_Gravel_SoundSet"};
			};
		};
		class HoofMediumWalk_LookupTable
		{
			class HoofMediumWalkstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumWalk_Grass_dry_SoundSet"};
			};
			class HoofMediumWalkstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumWalk_Gravel_SoundSet"};
			};
		};
		class HoofMediumRun_LookupTable
		{
			class HoofMediumRunstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumRun_Grass_dry_SoundSet"};
			};
			class HoofMediumRunstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumRun_Gravel_SoundSet"};
			};
		};
		class HoofMediumGrazing_LookupTable
		{
			class HoofMediumGrazingstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumGrazing_Grass_dry_SoundSet"};
			};
			class HoofMediumGrazingstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumGrazing_Gravel_SoundSet"};
			};
		};
		class HoofMediumBodyfall_LookupTable
		{
			class HoofMediumBodyfallstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumBodyfall_Grass_dry_SoundSet"};
			};
			class HoofMediumBodyfallstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumBodyfall_Gravel_SoundSet"};
			};
		};
		class HoofMediumSettle_LookupTable
		{
			class HoofMediumSettlestepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumSettle_Grass_dry_SoundSet"};
			};
			class HoofMediumSettlestepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumSettle_Gravel_SoundSet"};
			};
		};
		class HoofMediumRest2standA_LookupTable
		{
			class HoofMediumRest2standAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumRest2standA_Grass_dry_SoundSet"};
			};
			class HoofMediumRest2standAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumRest2standA_Gravel_SoundSet"};
			};
		};
		class HoofMediumRest2standB_LookupTable
		{
			class HoofMediumRest2standBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumRest2standB_Grass_dry_SoundSet"};
			};
			class HoofMediumRest2standBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumRest2standB_Gravel_SoundSet"};
			};
		};
		class HoofMediumStand2restA_LookupTable
		{
			class HoofMediumStand2restAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumStand2restA_Grass_dry_SoundSet"};
			};
			class HoofMediumStand2restAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumStand2restA_Gravel_SoundSet"};
			};
		};
		class HoofMediumStand2restB_LookupTable
		{
			class HoofMediumStand2restBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumStand2restB_Grass_dry_SoundSet"};
			};
			class HoofMediumStand2restBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumStand2restB_Gravel_SoundSet"};
			};
		};
		class HoofMediumStand2restC_LookupTable
		{
			class HoofMediumStand2restCstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumStand2restC_Grass_dry_SoundSet"};
			};
			class HoofMediumStand2restCstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumStand2restC_Gravel_SoundSet"};
			};
		};
		class HoofMediumRub1_LookupTable
		{
			class HoofMediumRub1stepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumRub1_Grass_dry_SoundSet"};
			};
			class HoofMediumRub1stepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumRub1_Gravel_SoundSet"};
			};
		};
		class HoofMediumRub2_LookupTable
		{
			class HoofMediumRub2stepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofMediumRub2_Grass_dry_SoundSet"};
			};
			class HoofMediumRub2stepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofMediumRub2_Gravel_SoundSet"};
			};
		};
		class HoofSmallWalk_LookupTable
		{
			class HoofSmallWalkstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallWalk_Grass_dry_SoundSet"};
			};
			class HoofSmallWalkstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallWalk_Gravel_SoundSet"};
			};
		};
		class HoofSmallRun_LookupTable
		{
			class HoofSmallRunstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallRun_Grass_dry_SoundSet"};
			};
			class HoofSmallRunstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallRun_Gravel_SoundSet"};
			};
		};
		class HoofSmallGrazing_LookupTable
		{
			class HoofSmallGrazingstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallGrazing_Grass_dry_SoundSet"};
			};
			class HoofSmallGrazingstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallGrazing_Gravel_SoundSet"};
			};
		};
		class HoofSmallGrazingHard_LookupTable
		{
			class HoofSmallGrazingHardstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallGrazingHard_Grass_dry_SoundSet"};
			};
			class HoofSmallGrazingHardstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallGrazingHard_Gravel_SoundSet"};
			};
		};
		class HoofSmallGrazingLeave_LookupTable
		{
			class HoofSmallGrazingLeavestepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallGrazingLeave_Grass_dry_SoundSet"};
			};
			class HoofSmallGrazingLeavestepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallGrazingLeave_Gravel_SoundSet"};
			};
		};
		class HoofSmallBodyfall_LookupTable
		{
			class HoofSmallBodyfallstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallBodyfall_Grass_dry_SoundSet"};
			};
			class HoofSmallBodyfallstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallBodyfall_Gravel_SoundSet"};
			};
		};
		class HoofSmallSettle_LookupTable
		{
			class HoofSmallSettlestepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallSettle_Grass_dry_SoundSet"};
			};
			class HoofSmallSettlestepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallSettle_Gravel_SoundSet"};
			};
		};
		class HoofSmallRest2standA_LookupTable
		{
			class HoofSmallRest2standAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallRest2standA_Grass_dry_SoundSet"};
			};
			class HoofSmallRest2standAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallRest2standA_Gravel_SoundSet"};
			};
		};
		class HoofSmallRest2standB_LookupTable
		{
			class HoofSmallRest2standBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallRest2standB_Grass_dry_SoundSet"};
			};
			class HoofSmallRest2standBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallRest2standB_Gravel_SoundSet"};
			};
		};
		class HoofSmallStand2restA_LookupTable
		{
			class HoofSmallStand2restAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallStand2restA_Grass_dry_SoundSet"};
			};
			class HoofSmallStand2restAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallStand2restA_Gravel_SoundSet"};
			};
		};
		class HoofSmallStand2restB_LookupTable
		{
			class HoofSmallStand2restBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallStand2restB_Grass_dry_SoundSet"};
			};
			class HoofSmallStand2restBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallStand2restB_Gravel_SoundSet"};
			};
		};
		class HoofSmallStand2restC_LookupTable
		{
			class HoofSmallStand2restCstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallStand2restC_Grass_dry_SoundSet"};
			};
			class HoofSmallStand2restCstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallStand2restC_Gravel_SoundSet"};
			};
		};
		class HoofSmallRub1_LookupTable
		{
			class HoofSmallRub1stepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallRub1_Grass_dry_SoundSet"};
			};
			class HoofSmallRub1stepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallRub1_Gravel_SoundSet"};
			};
		};
		class HoofSmallRub2_LookupTable
		{
			class HoofSmallRub2stepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"HoofSmallRub2_Grass_dry_SoundSet"};
			};
			class HoofSmallRub2stepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"HoofSmallRub2_Gravel_SoundSet"};
			};
		};
		class PawBigWalk_LookupTable
		{
			class PawBigWalkstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigWalk_Grass_dry_SoundSet"};
			};
			class PawBigWalkstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigWalk_Gravel_SoundSet"};
			};
		};
		class PawBigRun_LookupTable
		{
			class PawBigRunstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigRun_Grass_dry_SoundSet"};
			};
			class PawBigRunstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigRun_Gravel_SoundSet"};
			};
		};
		class PawBigGrazing_LookupTable
		{
			class PawBigGrazingstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigGrazing_Grass_dry_SoundSet"};
			};
			class PawBigGrazingstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigGrazing_Gravel_SoundSet"};
			};
		};
		class PawBigBodyfall_LookupTable
		{
			class PawBigBodyfallstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigBodyfall_Grass_dry_SoundSet"};
			};
			class PawBigBodyfallstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigBodyfall_Gravel_SoundSet"};
			};
		};
		class PawBigSettle_LookupTable
		{
			class PawBigSettlestepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigSettle_Grass_dry_SoundSet"};
			};
			class PawBigSettlestepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigSettle_Gravel_SoundSet"};
			};
		};
		class PawBigRest2standA_LookupTable
		{
			class PawBigRest2standAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigRest2standA_Grass_dry_SoundSet"};
			};
			class PawBigRest2standAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigRest2standA_Gravel_SoundSet"};
			};
		};
		class PawBigRest2standB_LookupTable
		{
			class PawBigRest2standBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigRest2standB_Grass_dry_SoundSet"};
			};
			class PawBigRest2standBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigRest2standB_Gravel_SoundSet"};
			};
		};
		class PawBigStand2restA_LookupTable
		{
			class PawBigStand2restAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigStand2restA_Grass_dry_SoundSet"};
			};
			class PawBigStand2restAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigStand2restA_Gravel_SoundSet"};
			};
		};
		class PawBigStand2restB_LookupTable
		{
			class PawBigStand2restBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigStand2restB_Grass_dry_SoundSet"};
			};
			class PawBigStand2restBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigStand2restB_Gravel_SoundSet"};
			};
		};
		class PawBigStand2restC_LookupTable
		{
			class PawBigStand2restCstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigStand2restC_Grass_dry_SoundSet"};
			};
			class PawBigStand2restCstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigStand2restC_Gravel_SoundSet"};
			};
		};
		class PawBigJump_LookupTable
		{
			class PawBigJumpstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigJump_Grass_dry_SoundSet"};
			};
			class PawBigJumpstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigJump_Gravel_SoundSet"};
			};
		};
		class PawBigImpact_LookupTable
		{
			class PawBigImpactstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawBigImpact_Grass_dry_SoundSet"};
			};
			class PawBigImpactstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawBigImpact_Gravel_SoundSet"};
			};
		};
		class PawMediumWalk_LookupTable
		{
			class PawMediumWalkstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumWalk_Grass_dry_SoundSet"};
			};
			class PawMediumWalkstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumWalk_Gravel_SoundSet"};
			};
		};
		class PawMediumRun_LookupTable
		{
			class PawMediumRunstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumRun_Grass_dry_SoundSet"};
			};
			class PawMediumRunstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumRun_Gravel_SoundSet"};
			};
		};
		class PawMediumGrazing_LookupTable
		{
			class PawMediumGrazingstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumGrazing_Grass_dry_SoundSet"};
			};
			class PawMediumGrazingstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumGrazing_Gravel_SoundSet"};
			};
		};
		class PawMediumBodyfall_LookupTable
		{
			class PawMediumBodyfallstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumBodyfall_Grass_dry_SoundSet"};
			};
			class PawMediumBodyfallstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumBodyfall_Gravel_SoundSet"};
			};
		};
		class PawMediumSettle_LookupTable
		{
			class PawMediumSettlestepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumSettle_Grass_dry_SoundSet"};
			};
			class PawMediumSettlestepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumSettle_Gravel_SoundSet"};
			};
		};
		class PawMediumRest2standA_LookupTable
		{
			class PawMediumRest2standAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumRest2standA_Grass_dry_SoundSet"};
			};
			class PawMediumRest2standAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumRest2standA_Gravel_SoundSet"};
			};
		};
		class PawMediumRest2standB_LookupTable
		{
			class PawMediumRest2standBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumRest2standB_Grass_dry_SoundSet"};
			};
			class PawMediumRest2standBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumRest2standB_Gravel_SoundSet"};
			};
		};
		class PawMediumStand2restA_LookupTable
		{
			class PawMediumStand2restAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumStand2restA_Grass_dry_SoundSet"};
			};
			class PawMediumStand2restAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumStand2restA_Gravel_SoundSet"};
			};
		};
		class PawMediumStand2restB_LookupTable
		{
			class PawMediumStand2restBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumStand2restB_Grass_dry_SoundSet"};
			};
			class PawMediumStand2restBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumStand2restB_Gravel_SoundSet"};
			};
		};
		class PawMediumStand2restC_LookupTable
		{
			class PawMediumStand2restCstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumStand2restC_Grass_dry_SoundSet"};
			};
			class PawMediumStand2restCstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumStand2restC_Gravel_SoundSet"};
			};
		};
		class PawMediumJump_LookupTable
		{
			class PawMediumJumpstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumJump_Grass_dry_SoundSet"};
			};
			class PawMediumJumpstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumJump_Gravel_SoundSet"};
			};
		};
		class PawMediumImpact_LookupTable
		{
			class PawMediumImpactstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawMediumImpact_Grass_dry_SoundSet"};
			};
			class PawMediumImpactstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawMediumImpact_Gravel_SoundSet"};
			};
		};
		class PawSmallWalk_LookupTable
		{
			class PawSmallWalkstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallWalk_Grass_dry_SoundSet"};
			};
			class PawSmallWalkstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallWalk_Gravel_SoundSet"};
			};
		};
		class PawSmallRun_LookupTable
		{
			class PawSmallRunstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallRun_Grass_dry_SoundSet"};
			};
			class PawSmallRunstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallRun_Gravel_SoundSet"};
			};
		};
		class PawSmallGrazing_LookupTable
		{
			class PawSmallGrazingstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallGrazing_Grass_dry_SoundSet"};
			};
			class PawSmallGrazingstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallGrazing_Gravel_SoundSet"};
			};
		};
		class PawSmallBodyfall_LookupTable
		{
			class PawSmallBodyfallstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallBodyfall_Grass_dry_SoundSet"};
			};
			class PawSmallBodyfallstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallBodyfall_Gravel_SoundSet"};
			};
		};
		class PawSmallSettle_LookupTable
		{
			class PawSmallSettlestepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallSettle_Grass_dry_SoundSet"};
			};
			class PawSmallSettlestepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallSettle_Gravel_SoundSet"};
			};
		};
		class PawSmallRest2standA_LookupTable
		{
			class PawSmallRest2standAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallRest2standA_Grass_dry_SoundSet"};
			};
			class PawSmallRest2standAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallRest2standA_Gravel_SoundSet"};
			};
		};
		class PawSmallRest2standB_LookupTable
		{
			class PawSmallRest2standBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallRest2standB_Grass_dry_SoundSet"};
			};
			class PawSmallRest2standBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallRest2standB_Gravel_SoundSet"};
			};
		};
		class PawSmallStand2restA_LookupTable
		{
			class PawSmallStand2restAstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallStand2restA_Grass_dry_SoundSet"};
			};
			class PawSmallStand2restAstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallStand2restA_Gravel_SoundSet"};
			};
		};
		class PawSmallStand2restB_LookupTable
		{
			class PawSmallStand2restBstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallStand2restB_Grass_dry_SoundSet"};
			};
			class PawSmallStand2restBstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallStand2restB_Gravel_SoundSet"};
			};
		};
		class PawSmallStand2restC_LookupTable
		{
			class PawSmallStand2restCstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallStand2restC_Grass_dry_SoundSet"};
			};
			class PawSmallStand2restCstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallStand2restC_Gravel_SoundSet"};
			};
		};
		class PawSmallJump_LookupTable
		{
			class PawSmallJumpstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallJump_Grass_dry_SoundSet"};
			};
			class PawSmallJumpstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallJump_Gravel_SoundSet"};
			};
		};
		class PawSmallImpact_LookupTable
		{
			class PawSmallImpactstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"PawSmallImpact_Grass_dry_SoundSet"};
			};
			class PawSmallImpactstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"PawSmallImpact_Gravel_SoundSet"};
			};
		};
		class bodyfall_Zmb_LookupTable
		{
			class bodyfall_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_grass_Zmb_SoundSet"};
			};
			class bodyfall_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_gravelSmall_ext_Zmb_SoundSet"};
			};
		};
		class bodyfall_hand_Zmb_LookupTable
		{
			class bodyfall_hand_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_hand_grass_Zmb_SoundSet"};
			};
			class bodyfall_hand_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_hand_gravelSmall_ext_Zmb_SoundSet"};
			};
		};
		class bodyfall_slide_Zmb_LookupTable
		{
			class bodyfall_slide_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_slide_grass_Zmb_SoundSet"};
			};
			class bodyfall_slide_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_slide_gravelSmall_ext_Zmb_SoundSet"};
			};
		};
		class walkErc_Bare_Zmb_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class runErc_Bare_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class sprintErc_Bare_Zmb_LookupTable
		{
			class sprintErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"sprintErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class sprintErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"sprintErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class landFeetErc_Bare_Zmb_LookupTable
		{
			class landFeetErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"landFeetErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class landFeetErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"landFeetErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class scuffErc_Bare_Zmb_LookupTable
		{
			class scuffErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"scuffErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class scuffErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"scuffErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class walkRasErc_Bare_Zmb_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class runRasErc_Bare_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class landFootErc_Bare_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class walkCro_Bare_Zmb_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class runCro_Bare_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class jumpErc_Bare_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_bare_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_bare_Zmb_Soundset"};
			};
		};
		class walkErc_Boots_Zmb_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class runErc_Boots_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class sprintErc_Boots_Zmb_LookupTable
		{
			class sprintErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"sprintErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class sprintErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"sprintErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class landFeetErc_Boots_Zmb_LookupTable
		{
			class landFeetErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"landFeetErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class landFeetErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"landFeetErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class scuffErc_Boots_Zmb_LookupTable
		{
			class scuffErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"scuffErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class scuffErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"scuffErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class walkRasErc_Boots_Zmb_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class runRasErc_Boots_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class landFootErc_Boots_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class walkCro_Boots_Zmb_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class runCro_Boots_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class jumpErc_Boots_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_boots_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_boots_Zmb_Soundset"};
			};
		};
		class walkErc_Sneakers_Zmb_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class runErc_Sneakers_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class sprintErc_Sneakers_Zmb_LookupTable
		{
			class sprintErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"sprintErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class sprintErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"sprintErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class landFeetErc_Sneakers_Zmb_LookupTable
		{
			class landFeetErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"landFeetErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class landFeetErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"landFeetErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class scuffErc_Sneakers_Zmb_LookupTable
		{
			class scuffErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"scuffErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class scuffErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"scuffErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class walkRasErc_Sneakers_Zmb_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class runRasErc_Sneakers_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class landFootErc_Sneakers_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class walkCro_Sneakers_Zmb_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class runCro_Sneakers_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class jumpErc_Sneakers_Zmb_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_sneakers_Zmb_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_sneakers_Zmb_Soundset"};
			};
		};
		class walkProne_Zmb_LookupTable
		{
			class walkProne_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkProne_grass_dry_ext_Zmb_Soundset"};
			};
			class walkProne_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkProne_gravel_small_ext_Zmb_Soundset"};
			};
		};
		class runProne_Zmb_LookupTable
		{
			class runProne_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runProne_grass_dry_ext_Zmb_Soundset"};
			};
			class runProne_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runProne_gravel_small_ext_Zmb_Soundset"};
			};
		};
		class walkErc_Char_LookupTable
		{
			class walkErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkErc_grass_dry_ext_Char_Soundset"};
			};
			class walkErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkErc_gravel_small_ext_Char_Soundset"};
			};
		};
		class walkRasErc_Char_LookupTable
		{
			class walkRasErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkRasErc_grass_dry_ext_Char_Soundset"};
			};
			class walkRasErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkRasErc_gravel_small_ext_Char_Soundset"};
			};
		};
		class runErc_Char_LookupTable
		{
			class runErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runErc_grass_dry_ext_Char_Soundset"};
			};
			class runErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runErc_gravel_small_ext_Char_Soundset"};
			};
		};
		class runRasErc_Char_LookupTable
		{
			class runRasErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runRasErc_grass_dry_ext_Char_Soundset"};
			};
			class runRasErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runRasErc_gravel_small_ext_Char_Soundset"};
			};
		};
		class sprintErc_Char_LookupTable
		{
			class sprintErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"sprintErc_grass_dry_ext_Char_Soundset"};
			};
			class sprintErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"sprintErc_gravel_small_ext_Char_Soundset"};
			};
		};
		class landFootErc_Char_LookupTable
		{
			class landFootErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"landFootErc_grass_dry_ext_Char_Soundset"};
			};
			class landFootErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"landFootErc_gravel_small_ext_Char_Soundset"};
			};
		};
		class landFeetErc_Char_LookupTable
		{
			class landFeetErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"landFeetErc_grass_dry_ext_Char_Soundset"};
			};
			class landFeetErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"landFeetErc_gravel_small_ext_Char_Soundset"};
			};
		};
		class scuffErc_Char_LookupTable
		{
			class scuffErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"scuffErc_grass_dry_ext_Char_Soundset"};
			};
			class scuffErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"scuffErc_gravel_small_ext_Char_Soundset"};
			};
		};
		class walkCro_Char_LookupTable
		{
			class walkCro_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkCro_grass_dry_ext_Char_Soundset"};
			};
			class walkCro_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkCro_gravel_small_ext_Char_Soundset"};
			};
		};
		class runCro_Char_LookupTable
		{
			class runCro_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runCro_grass_dry_ext_Char_Soundset"};
			};
			class runCro_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runCro_gravel_small_ext_Char_Soundset"};
			};
		};
		class jumpErc_Char_LookupTable
		{
			class jumpErc_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"jumpErc_grass_dry_ext_Char_Soundset"};
			};
			class jumpErc_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"jumpErc_gravel_small_ext_Char_Soundset"};
			};
		};
		class walkProne_Char_LookupTable
		{
			class walkProne_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkProne_grass_dry_ext_Char_Soundset"};
			};
			class walkProne_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkProne_gravel_small_ext_Char_Soundset"};
			};
		};
		class runProne_Char_LookupTable
		{
			class runProne_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"runProne_grass_dry_ext_Char_Soundset"};
			};
			class runProne_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"runProne_gravel_small_ext_Char_Soundset"};
			};
		};
		class walkProne_noHS_Char_LookupTable
		{
			class walkProne_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkProne_noHS_grass_dry_ext_Char_Soundset"};
			};
			class walkProne_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkProne_noHS_gravel_small_ext_Char_Soundset"};
			};
		};
		class walkProneLong_noHS_Char_LookupTable
		{
			class walkProneLong_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"walkProneLong_noHS_grass_dry_ext_Char_Soundset"};
			};
			class walkProneLong_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"walkProneLong_noHS_gravel_small_ext_Char_Soundset"};
			};
		};
		class handstepSound_Char_LookupTable
		{
			class handstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"Handstep_grass_dry_ext_Char_SoundSet"};
			};
			class handstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"Handstep_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class handstepSound_Hard_Char_LookupTable
		{
			class handstepSound_Hard_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"Handstep_Hard_grass_dry_ext_Char_SoundSet"};
			};
			class handstepSound_Hard_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"Handstep_Hard_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class handsstepSound_Char_LookupTable
		{
			class handsstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"Handsstep_grass_dry_ext_Char_SoundSet"};
			};
			class handsstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"Handsstep_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class bodyfallSound_Char_LookupTable
		{
			class bodyfallSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_grass_Char_SoundSet"};
			};
			class bodyfallSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class bodyfall_handSound_Char_LookupTable
		{
			class bodyfall_handSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_hand_grass_Char_SoundSet"};
			};
			class bodyfall_handSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_hand_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class bodyfall_rollSound_Char_LookupTable
		{
			class bodyfall_rollSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_roll_grass_Char_SoundSet"};
			};
			class bodyfall_rollSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_roll_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class bodyfall_rollHardSound_Char_LookupTable
		{
			class bodyfall_rollHardSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_rollHard_grass_Char_SoundSet"};
			};
			class bodyfall_rollHardSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_rollHard_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class bodyfall_slideSound_Char_LookupTable
		{
			class bodyfall_slideSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_slide_grass_Char_SoundSet"};
			};
			class bodyfall_slideSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_slide_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class bodyfall_slide_lightSound_Char_LookupTable
		{
			class bodyfall_slide_lightSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_slide_light_grass_Char_SoundSet"};
			};
			class bodyfall_slide_lightSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_slide_light_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class bodyfall_hand_lightSound_Char_LookupTable
		{
			class bodyfall_hand_lightSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"bodyfall_hand_light_grass_Char_SoundSet"};
			};
			class bodyfall_hand_lightSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"bodyfall_hand_light_gravelSmall_ext_Char_SoundSet"};
			};
		};
		class step_ladder_Char_LookupTable
		{
			class handstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"step_ladder_Char_Soundset"};
			};
			class handstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"step_ladder_Char_Soundset"};
			};
		};
		class step_ladder_run_Char_LookupTable
		{
			class handstepSound_si_grass
			{
				surface = "si_grass";
				soundSets[] = {"step_ladder_run_Char_Soundset"};
			};
			class handstepSound_si_sand
			{
				surface = "si_sand";
				soundSets[] = {"step_ladder_run_Char_Soundset"};
			};
		};